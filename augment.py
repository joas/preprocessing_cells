import cv2
import numpy as np

def add_gaussian_noise(X_imgs,i):

    for path in X_imgs:
        # for i in range(10):
        print(path)
        # path = X_imgs[0]
        X_img = cv2.imread(path)
        gaussian_noise_imgs = []
        row, col, _ = X_img.shape
        # Gaussian distribution parameters
        mean = 0
        var = 0.1
        sigma = var ** 0.5
        X_img = X_img.astype(np.float32)
        gaussian = np.random.random((row, col, 1)).astype(np.float32)
        gaussian = np.concatenate((gaussian, gaussian, gaussian), axis = 2)
        gaussian_img = cv2.addWeighted(X_img, 0.75, 0.25 * gaussian, 0.25, 0)
        gaussian_noise_imgs.append(gaussian_img)
        gaussian_noise_imgs = np.array(gaussian_noise_imgs, dtype = np.float32)
        path = path.split('/')[-1]
        path = f'{path}_noised_{i}_.jpg'
        print(f'saving image to {path}')
        cv2.imwrite(f'{i}_{path}_noised.jpg', gaussian_img)
    return gaussian_noise_imgs
if __name__== '__main__':
    import glob

    X_imgs = glob.glob('data/*.jpg')
    for i in range(10):
        out = add_gaussian_noise(X_imgs, i+1)

