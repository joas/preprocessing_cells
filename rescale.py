import cv2
from PIL import Image
import glob
import os
import sys
import logging
import numpy as np


def main():
    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    in_path = sys.argv[1]
    file_ext = sys.argv[2]
    target_size = (int(sys.argv[3]), int(sys.argv[4]))  # 1280,960
    out_path = sys.argv[5]
    # target_size = (1280, 960)  # width, height format
    img_list = glob.glob('*.' + file_ext, root_dir=in_path)

    for img_path in img_list:
        # img1 = cv2.imread(os.path.join(in_path, img_path))
        img = Image.open(os.path.join(in_path, img_path)).convert("L")
        # numpy array as row, col format, so height, width
        pixel_shape = np.array(img).shape[0:2]
        if not (pixel_shape[1], pixel_shape[0]) == target_size:

            logger.info(f' rescaling for {img_path} with shape {pixel_shape}')
            logger.info(f'Resizing for {img_path}')
            img = img.resize(target_size)
        img.getexif().clear()
        if not os.path.exists(os.path.join(out_path,f'{sys.argv[3]}_{sys.argv[4]}')):
            os.mkdir(os.path.join(out_path,f'{sys.argv[3]}_{sys.argv[4]}'))
        img.save(os.path.join(out_path,f'{sys.argv[3]}_{sys.argv[4]}', img_path))


if __name__ == '__main__':
    main()
